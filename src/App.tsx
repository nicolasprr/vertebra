import { Layout, Tabs } from "antd";
import CardList from "./components/CardList";
import EcoEficiencia from "./components/EcoEficiencia";
import EmptyImage from "./components/EmptyImage";
import { TabProvider } from "./context/tabs";

const { Header, Footer, Sider, Content } = Layout;

const SiderStyle: React.CSSProperties = {
  backgroundColor: "white",
  overflowY: "scroll",
  padding: 8,
};

const HeaderStyle: React.CSSProperties = {
  backgroundColor: "white",
};

function App() {
  return (
    <TabProvider>
      <Layout style={{ height: "100vh", margin: -8 }}>
        <Header style={HeaderStyle}>
          <EcoEficiencia />
        </Header>
        <Layout>
          <Sider style={SiderStyle} width="30%">
            <CardList />
          </Sider>
          <Content>
            <EmptyImage />
          </Content>
        </Layout>
        <Footer />
      </Layout>
    </TabProvider>
  );
}

export default App;
