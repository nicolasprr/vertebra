import { dbSources, shopIdType } from "../constants/dbSources";
import { TypeTab } from "../context/tabs/types";

export const getDataById = (
  choosedType: TypeTab,
  id: number | string,
  current: TypeTab
): Promise<any[]> => {
  const promise = new Promise<any>((resolve) => {
    console.log(
      `looking for ${choosedType} from ${current} with  ${dbSources[current]} equal  ${id}`
    );

    switch (choosedType) {
      case TypeTab.ACCOUNT:
        import("../db/Account.json").then((data) => {
          const filtered = data.data.filter(
            (account) => account[dbSources[current]] === id
          );
          resolve(filtered);
        });

        break;

      case TypeTab.GROUP:
        import("../db/Groups.json").then((data) => {
          const filtered = data.data.filter((group) => group.idCompany === id);
          resolve(filtered);
        });

        break;

      case TypeTab.SHOPS:
        import("../db/Shops.json").then((data) => {
          const currentSource = dbSources[current] as shopIdType;
          const filtered = data.data.filter(
            (group) => group[currentSource] === id
          );
          resolve(filtered);
        });

        break;
    }
  });
  return promise;
};
