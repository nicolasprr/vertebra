// import { useState, useReducer } from "react";
import { Tabs } from "antd";
// import EcoTabCompany from "./EcoTabCompany";
// import { EcoTabContext } from "./context/EcoTabContext";
// import {
//   EcoTabReducer,
//   EcoTabReducerActionType,
// } from "./context/EcoTabReducer";
import { useTab } from "../context/tabs";
import { changeIndexAction, deleteAction } from "../context/tabs/actions";
import { ITab } from "../context/tabs/types";

const { TabPane } = Tabs;

const EcoEficiencia = () => {
  const { dispatch, value } = useTab();

  const { tabs, currentIndex } = value;

  const handleChangeTab = (idx: string) => {
    dispatch(changeIndexAction(Number(idx)));
  };

  const handleRemoveTab = (idx: string) => {
    
    if(currentIndex === Number(idx)){
      dispatch(changeIndexAction(0));

    }
    dispatch(deleteAction(Number(idx)));
  };

  return (
    <>
      <Tabs
        hideAdd
        type="editable-card"
        onChange={handleChangeTab}
        activeKey={String(currentIndex)}
        onEdit={(id) => handleRemoveTab(id as string)}
      >
        {/* <TabPane closable={false} tab="COMPANIA"></TabPane> */}

        {tabs.map((pane: ITab, i) => (
          <TabPane tab={pane.name} key={i} closable={pane.metadata.closable} />
        ))}
      </Tabs>
    </>
  );
};

export default EcoEficiencia;
