import { Result } from "antd";
import { useTab } from "../context/tabs";
import { TypeTab } from "../context/tabs/types";

const EmptyImage = () => {
  const { value } = useTab();
  const { currentIndex, tabs } = value;

  const currentTab = tabs[currentIndex];

  const currentType = currentTab?.metadata?.type as TypeTab;

  const isEmpty = currentTab?.records?.length === 0;

  if (isEmpty) {
    return (
      <Result
        status="404"
        title="404"
        subTitle={`No hay ${currentType.toLowerCase()} en ${currentTab.name}`}
      />
    );
  }
  return null;
};

export default EmptyImage;
