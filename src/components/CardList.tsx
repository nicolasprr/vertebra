import { menuNames } from "../constants/menuNames";
import { useTab } from "../context/tabs";
import { TypeTab } from "../context/tabs/types";
import { addAction } from "../context/tabs/actions";
import EcoCard from "./EcoCard";
import { getDataById } from "../utils/db";

const CardList = () => {
  const { value, dispatch } = useTab();
  const { currentIndex, tabs } = value;

  const currentTab = tabs[currentIndex];

  const currentType = currentTab?.metadata.type as TypeTab;

  const options = menuNames[currentType];

  const handleClickMenu = async (
    choosedType: TypeTab,
    id: number | string,
    name: string
  ) => {
    const records = await getDataById(choosedType, id, currentType);

    dispatch(
      addAction({
        records,
        name: `${name} - ${choosedType}`,
        metadata: {
          type: choosedType,
          closable: true,
        },
      })
    );
  };

  return (
    <>
      {tabs[currentIndex]?.records.map((item) => (
        <EcoCard
          onClickMenu={handleClickMenu}
          options={options}
          record={item}
          key={item.id}
        />
      ))}
    </>
  );
};

export default CardList;
