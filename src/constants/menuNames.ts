import { TypeTab } from "../context/tabs/types";

export const menuNames = {
  [TypeTab.COMPANY]: [
    { label: TypeTab.GROUP, key: TypeTab.GROUP },
    { label: TypeTab.SHOPS, key: TypeTab.SHOPS },
    { label: TypeTab.ACCOUNT, key: TypeTab.ACCOUNT },
  ],
  [TypeTab.ACCOUNT]: [
    // { label: TypeTab.GROUP, key: TypeTab.GROUP },
    // { label: TypeTab.SHOPS, key: TypeTab.SHOPS },
    // { label: TypeTab.ACCOUNT, key: TypeTab.ACCOUNT },
  ],
  [TypeTab.GROUP]: [
    { label: TypeTab.SHOPS, key: TypeTab.SHOPS },
    { label: TypeTab.ACCOUNT, key: TypeTab.ACCOUNT },
  ],
  [TypeTab.SHOPS]: [{ label: TypeTab.ACCOUNT, key: TypeTab.ACCOUNT }],
};
