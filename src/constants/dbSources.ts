import { TypeTab } from "../context/tabs/types";

const idKeys = ["idGroup", "idCompany", "idShop"] as const;
export type idTypes = typeof idKeys[number];

const shopsKeys = [idKeys[0], idKeys[1]];
export type shopIdType = typeof shopsKeys[number];

export const dbSources: Record<TypeTab, idTypes> = {
  [TypeTab.GROUP]: idKeys[0],
  [TypeTab.COMPANY]: idKeys[1],
  [TypeTab.SHOPS]: idKeys[2],
  [TypeTab.ACCOUNT]: idKeys[2],
};
