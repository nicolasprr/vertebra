import { Action, ITab, TabAction } from "./types";

export const addAction = (item: ITab): Action => {
  return { type: TabAction.ADD, payload: item };
};

export const deleteAction = (item: number): Action => {
  return { type: TabAction.DELETE, payload: item };
};

export const changeIndexAction = (number: number): Action => {
  return { type: TabAction.CHANGE_INDEX, payload: number };
};
