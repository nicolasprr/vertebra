import { TabAction, Action, State, TypeTab } from "./types";

import CompanyData from "../../db/Company.json";

export const initialState: State = {
  currentIndex: 0,
  tabs: [
    {
      name: TypeTab.COMPANY,
      records: CompanyData.data,
      metadata: {
        type: TypeTab.COMPANY,
        closable: false,
      },
    },
  ],
};

const tabReducer = (state = initialState, action: Action): State => {
  switch (action.type) {
    case TabAction.ADD:
      return { ...state, tabs: [...state.tabs, action.payload] };
    case TabAction.DELETE:
      return {
        ...state,
        tabs: state.tabs.filter((_, i) => i !== action.payload),
      };
    case TabAction.CHANGE_INDEX:
      return {
        ...state,
        currentIndex: action.payload,
      };
    default:
      return state;
  }
};

export default tabReducer;
