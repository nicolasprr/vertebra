export enum TabAction {
  ADD = "ADD",
  DELETE = "DELETE",
  CHANGE_INDEX = "CHANGE_INDEX",
}

export enum TypeTab {
  ACCOUNT = "Cuentas",
  GROUP = "Grupos",
  COMPANY = "Compañias",
  SHOPS = "Tiendas",
}

interface AddTab {
  type: typeof TabAction.ADD;
  payload: any;
}

interface DeleteTab {
  type: typeof TabAction.DELETE;
  payload: any;
}

interface ChangeIndex {
  type: typeof TabAction.CHANGE_INDEX;
  payload: number;
}

export type Action = DeleteTab | AddTab | ChangeIndex;

export type Dispatch = (val: Action) => void;

export interface ITab {
  name: string;
  records: any[];
  metadata: {
    type: TypeTab;
    closable: boolean;
  };
}

export interface State {
  currentIndex: number;
  tabs: ITab[];
}
