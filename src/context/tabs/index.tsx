import { createContext, ReactNode, useContext, useReducer } from "react";
import tabReducer, { initialState } from "./reducer";
import { Dispatch, State } from "./types";

const TabContext = createContext<{ dispatch: Dispatch; value: State }>({
  value: initialState,
  dispatch: () => null,
});

interface Props {
  children: ReactNode;
}

export const TabProvider = ({ children }: Props) => {
  const [value, dispatch] = useReducer(tabReducer, initialState);

  return (
    <TabContext.Provider value={{ value, dispatch }}>
      {children}
    </TabContext.Provider>
  );
};

export const useTab = () => useContext(TabContext);
